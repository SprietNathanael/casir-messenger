# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals

from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import JsonResponse

from members.models import Member
from messaging.models import Message
from members.forms import AddFriendForm
from members.forms import PendingRequestForm
from messaging.forms import SendMessageForm


@login_required
def index(request):
    current_member = request.user.member

    if request.method == "POST":
        add_friend_form = AddFriendForm(
            member=current_member, data=request.POST)
        pending_request_form = PendingRequestForm(member=current_member, data=request.POST)
        send_message_form = SendMessageForm(
            member=current_member, data=request.POST, files=request.FILES)
        if "add_friend_request" in request.POST:
            if add_friend_form.is_valid():
                for member in add_friend_form.cleaned_data["members"]:
                    member.friend_requests.add(current_member)
                add_friend_form = AddFriendForm(member=current_member)

                messages.success(
                    request, "Demandes d'ajout effectuées avec succès")
        elif "send_message_request" in request.POST:
            if send_message_form.is_valid():
                author = current_member
                recipient = send_message_form.cleaned_data['recipient']
                content = send_message_form.cleaned_data['content']
                duration = send_message_form.cleaned_data['duration']
                picture = send_message_form.cleaned_data['picture']
                message = Message(
                    author=author, recipient=recipient, content=content, duration=duration, picture=picture)
                message.save()
                send_message_form = SendMessageForm(member=current_member)
        else:
            print(pending_request_form.is_valid())
            if pending_request_form.is_valid():
                if "refuse_pending_request" in request.POST:
                    for member in pending_request_form.cleaned_data["members"]:
                        current_member.friend_requests.remove(member)
                    messages.success(
                        request, "Demandes en attente refusées avec succès")
                elif "accept_pending_request" in request.POST:
                    for member in pending_request_form.cleaned_data["members"]:
                        current_member.friend_requests.remove(member)
                        current_member.friends.add(member)
                    messages.success(
                        request, "Demandes en attente acceptées avec succès")
            else:
                messages.warning(
                    request, "Demandes en attente : opération annulée")
    elif request.method == "GET":
        add_friend_form = AddFriendForm(member=current_member)
        pending_request_form = PendingRequestForm(member=current_member)
        send_message_form = SendMessageForm(member=current_member)

    friends = current_member.friends.all()
    friend_requests = current_member.friend_requests.all()
    recieved_messages = Message.objects.filter(recipient=current_member).order_by("-created_on")
    sender = request.GET.get("sender")
    if sender:
        recieved_messages = recieved_messages.filter(
            author__user__username=sender)
    paginator = Paginator(recieved_messages, 5)
    page = request.GET.get("page")
    messages_list = []
    try:
        messages_list = paginator.page(page)
    except PageNotAnInteger:
        messages_list = paginator.page(1)
    except EmptyPage:
        messages_list = paginator.page(paginator.num_pages)
    context = {'friends': friends,
               'friend_requests': friend_requests,
               'add_friend_form': add_friend_form,
               'pending_request_form': pending_request_form,
               'send_message_form': send_message_form,
               'messages_list': messages_list.object_list,
               'current_page': messages_list,
               'paginator': paginator,
               'sender': sender}

    return render(request, "dashboard/index.html", context)


@login_required
def getMessage(request, id_message):
    message_to_send = Message.objects.get(id=id_message)

    if message_to_send.is_read:
        return False

    message_to_send.is_read = True
    message_to_send.save()
    if(message_to_send.picture == ""):
        picture = "/static/img/no_image.png"
    else:
        picture = message_to_send.picture.url
    print(picture)
    data = {
        'picture': picture,
        'content': message_to_send.content,
        'duration': message_to_send.duration,
        'author': message_to_send.author.user.username
    }
    print(data)
    return JsonResponse(data)


@login_required
def remove_friend(request, friend_pk):
    current_member = request.user.member
    removed_friend = get_object_or_404(Member, pk=friend_pk)
    current_member.friends.remove(removed_friend)
    messages.success(request, "Suppression effectuée avec succès")
    return redirect("dashboard:index")
