# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals
from django.conf.urls import patterns, url
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = patterns('',
                       url(r'^index/$', 'dashboard.views.index', name="index"),
                       url('^getMessage/(?P<id_message>\d+)/$', 'dashboard.views.getMessage', name="getMessage"),
                       url(r'^remove/(?P<friend_pk>[\d]+)/$', 'dashboard.views.remove_friend', name="remove"),
                       )+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
