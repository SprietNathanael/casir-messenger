# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals
from django.forms.models import ModelForm
from messaging.models import Message


class SendMessageForm(ModelForm):
    class Meta:
        model = Message
        fields = ('recipient', 'content', 'picture', 'duration')

    def __init__(self, member, *args, **kwargs):
        super(SendMessageForm, self).__init__(*args, **kwargs)
        self.fields["recipient"].queryset = member.friends.all()
        self.fields["recipient"].widget.attrs['class'] = "col-lg-10 col-sd-10 col-md-10 col-xs-10 "
